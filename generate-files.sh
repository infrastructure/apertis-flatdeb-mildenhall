#!/bin/sh

set -e

if [ $# -ne 1 ]; then
  echo "Usage: $0 APERTIS_VERSION" >&2
  exit 1
fi

VERSION=$1
versioncheck=$(echo $VERSION | grep -o 'v[0-9]\{4\}\(dev[0-9]\|pre\)\?')
if [ "$versioncheck" != "$VERSION" ]; then
  echo "Error: wrong Apertis version format: should be 'vXXXX[devY|pre]' with" >&2
  echo "\tXXXX\tthe release year" >&2
  echo "\tY\ta single digit between 0 and 3" >&2
  exit 2
fi

projectdir=$(dirname $0)
cp ${projectdir}/suites/template.yaml ${projectdir}/suites/${VERSION}.yaml
